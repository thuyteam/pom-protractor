
exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    specs: [
        'test/features/register.feature',
        'test/features/login.feature',
        'test/features/application_management.feature'
    ],
    cucumberOpts: {
        require: ['hook.js','test/steps/*_steps.js'],
        format: 'pretty'
    }
}
