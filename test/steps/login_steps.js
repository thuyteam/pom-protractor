/** 
 * Created by nguyenthihongthuy on 30/08/17. 
 */
var LoginSteps = function () {

    var LoginPage = require("../pages/login_page.js");
    var DashboardPage = require("../pages/dashboard_page.js");

    this.Before(function (scenario, callback) {

        this.login_page = new LoginPage();
        this.dashboard_page = new DashboardPage();
        callback();
    });


    this.Then('I verify that login screen appears', function (callback) {
        this.login_page.verifyLoginScreenExisted();
        callback();
    });
    this.When('I type username as $username , password as $password', function (username, password, callback) {
        this.login_page.setUsername(username);
        this.login_page.setPassword(password);
        callback();
    });
    this.When('I click Login button', function (callback) {
        this.login_page.clickLogin();
        callback();
    });

    this.Then('I check dashboard screen appears', function (callback) {

        this.dashboard_page.verifyDashboardScreenAppears();
        this.dashboard_page.logout();
        callback();
    });

    this.Given('I login in $website with username $username, password $password successfully', function (website, username, password, callback) {


        this.login_page.loginSystemSuccessfully(website, username, password);
        this.dashboard_page.verifyDashboardScreenAppears();
        callback();
    });


};
module.exports = LoginSteps; 
