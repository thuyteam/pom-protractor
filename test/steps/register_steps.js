/**
 * Created by nguyenthihongthuy on 30/08/17.
 */


var RegisterSteps = function () {

    var RegisterPage = require("../pages/register_page.js");

    this.Before(function (scenario, callback) {
        this.register_page = new RegisterPage();
        callback();
    });

    this.Given('I open the website $website', function (website, callback) {
        this.register_page.getURL(website);
        callback();
    });

    this.Then('I verify that welcome page is appeared', function (callback) {
        //expectation: the system navigated to the Dashboard screen
        this.register_page.verifyWelcomePage();
        callback();
    });

    this.When('I click Register button', function (callback) {
        this.register_page.clickRegister();
        callback();
    });

    this.Then('I verify that register page is appeared', function (callback) {
        this.register_page.verifyRegisterPage();
        callback();
    });

    this.When('I register with firstname $firstname, lastname $lastname, username $username, password $password', function (firstname, lastname, username, password, callback) {
        this.register_page.typeUserInfo(firstname, lastname, username, password);
        callback();
    });


    this.Then('I verify that system shows the $message', function (message, callback) {
        this.register_page.verifyCreateAccount(message);
        callback();
    });

};

module.exports = RegisterSteps;