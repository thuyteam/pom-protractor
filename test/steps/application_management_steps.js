/**
 * Created by nguyenthihongthuy on 30/08/17.
 */

var ApplicationManagementSteps = function () {

    var ApplicationManagementPage = require("../pages/application_management_page.js");
    var DashboardPage = require("../pages/dashboard_page.js");

    this.Before(function (scenario, callback) {
        this.application_management_page = new ApplicationManagementPage();
        this.dashboard_page = new DashboardPage();
        callback();
    });

    this.Then('I navigate to Application page', function (callback) {
        this.dashboard_page.navigateApplicationPage();
        this.application_management_page.verifyApplicationPage();
        callback();
    });

    this.When('I click on Add Application button', function (callback) {

        this.application_management_page.clickAddApplication();
        callback();
    });

    this.When('I added an application with appname $appName, session expiration $session, userBase $userBase', function (appNam, session, userBase, callback) {

        this.application_management_page.addApplication(appNam, session, userBase);
        callback();
    });

    this.Then('I verify the application with $appname is added successfully', function (appname, callback) {
        this.application_management_page.verifyAddApplicationSuccessfully(appname);
       // this.dashboard_page.logout();
        callback();
    });

    this.When('I delete app name as $appName', function (appName, callback) {
        this.application_management_page.clickDelete(appName);
        this.application_management_page.clickConfirmDelete();
        callback();
    });

    this.Then('I verify the application with $appname is disappeared', function (appName, callback) {
        this.application_management_page.verifyDeleteApplicationSuccessfully(appName);
      //  this.dashboard_page.logout();
        callback();
    });

    this.When('I edit application infomation of $originalApplicationName with application name as $modifyApplicationName, Session Expiration as $sessionExpiration,User Base as $userBase', function (originalApplicationName, modifyApplicationName, sessionExpiration, userBase, callback) {
        this.application_management_page.editApplicationInfoSuccessfully(originalApplicationName, modifyApplicationName, sessionExpiration, userBase);
        callback();
    });

    this.Then('I verify the application with $originalApplicationName is edited successfully', function (originalApplicationName, callback) {
        this.application_management_page.verifyEditSuccessfully();
        //  this.dashboard_page.logout();
        callback();
    });

};

module.exports = ApplicationManagementSteps;


