/**
 * Created by nguyenthihongthuy on 30/08/17.
 */
var expect = require('./../../expect.js');
var RegisterPage = function () {
    //Register button in welcome page
    var registerBtn = element.all(by.linkText('Register')).get(0);
    var firstname = element(by.model('vm.user.firstName'));
    var lastname = element(by.model('vm.user.lastName'));
    var username = element(by.model('vm.user.username'));
    var password = element(by.model('vm.user.password'));
    //register button in register page
    var registerDetailBtn = element.all(by.buttonText('Register')).get(0);
    //message is showed after navigating to the home page
    var message = element(by.binding('flash.message'));

    this.getURL = function (value) {
        browser.get(value);
        browser.sleep(5000);
    };

    this.clickRegister = function () {
        registerBtn.click();
    }

    this.verifyWelcomePage = function () {
        //expectation: the system navigated to the Register screen
        expect(registerBtn).to.be.present;
    }

    this.verifyRegisterPage = function () {
        //expectation: the system navigated to the Register screen
        expect(firstname).to.be.present;
    }

    this.typeUserInfo = function (i_firstname, i_lastname, i_username, i_password) {
        firstname.sendKeys(i_firstname);
        lastname.sendKeys(i_lastname);
        username.sendKeys(i_username);
        password.sendKeys(i_password)

    }

    this.verifyCreateAccount = function (i_message) {

        registerDetailBtn.click().then(function () {
            message.getText().then(function (text) {
                expect(text).to.equal(i_message);
                browser.sleep(1500);
            });
        });
    }

};

module.exports = RegisterPage;
