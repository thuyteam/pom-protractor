var expect = require('./../../expect.js');
var DashboardPage = function () {
    var logoutBtn = element.all(by.linkText('Log out')).get(0);


    this.navigateApplicationPage = function () {
        element(by.model('vm.app.name')).isPresent().then(function (isPresent) {
            if(!isPresent)
                element(by.xpath('//*[@id="side-menu"]/li[14]/a/span[1]')).click();
                browser.sleep(5000);
                element(by.xpath('//*[@id="side-menu"]/li[14]/ul/li[1]/a')).click();
                browser.sleep(1000);
            });

    }

    this.verifyDashboardScreenAppears = function () {
        (element(by.id('btnAppSelect'))).isPresent();
    };


    this.logout = function () {
        browser.sleep(2000);
        logoutBtn.click();

    }



};

module.exports = DashboardPage;
