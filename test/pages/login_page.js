/**
 * Created by nguyenthihongthuy on 30/08/17.
 */
var expect = require('./../../expect.js');
var LoginPage = function() {

    this.getURL = function(value) {
        browser.get(value);
        browser.sleep(1000);
    };

    this.setUsername = function(value) {
        element(by.model('vm.username')).sendKeys(value);
        browser.sleep(1000);
    };

    this.setPassword = function(value) {
        element(by.model('vm.password')).sendKeys(value);
        browser.sleep(1000);
    };

    this.clickLogin = function() {
        element(by.buttonText('Login')).click();
        browser.sleep(1000);

    };

    this.verifyLoginScreenExisted = function() {
        (element(by.model('vm.username'))).isPresent();
    };

    this.loginSystemSuccessfully = function(website,i_username,i_password) {
        element(by.id('btnAppSelect')).isPresent().then(function (isPresent) {
            if(!isPresent){
                browser.get(website);
                browser.sleep(1000);
                element(by.model('vm.username')).sendKeys(i_username);
                element(by.model('vm.password')).sendKeys(i_password);
                element(by.buttonText('Login')).click();
                browser.sleep(1000);

            }
        });
    };


};

module.exports = LoginPage;
