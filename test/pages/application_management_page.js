var expect = require('./../../expect.js');
var ApplicationPage = function () {
    var appNameKeeper;
    var sessionExpirationKeeper;
    var userBaseKeeper;

    this.getURL = function (value) {
        browser.get(value);
        browser.sleep(5000);
    };

    this.clickAddApplication = function () {
        element.all(by.buttonText('Add Application')).click();
        browser.sleep(2000);

    };

    this.verifyApplicationPage = function () {
        element.all(by.buttonText('Add Application')).isPresent();
    }

    this.addApplication = function (appName, session, userBase) {
        element(by.model('vm.app.name')).sendKeys(appName);
        browser.sleep(500);
        element(by.model('vm.app.sessionExpiration')).sendKeys(session);
        browser.sleep(500);
        element(by.cssContainingText('option', userBase)).click();
        element.all(by.buttonText('Create')).click();
        browser.sleep(1000);

    }
;

    this.verifyAddApplicationSuccessfully = function (appName) {
        element.all(by.repeater('app in vm.allApps')).filter(function (rows) {
            return rows.all(by.css('td')).then(function (columns) {
                return columns[0].getText().then((function (text) {
                    return text === appName;
                }))
            })
        }).then(function (filterElement) {
            expect(filterElement.length > 0, 'App ' + appName + ' is not existed').to.be.equal(true);
        });


    }

    this.clickDelete = function (appName) {
        element.all(by.repeater('app in vm.allApps')).filter(function (rows) {
            return rows.all(by.css('td')).then(function (columns) {
                return columns[0].getText().then((function (text) {
                    return text === appName;
                }))
            })
        }).then(function (filterElements) {
            filterElements[0].element(by.linkText('Delete')).click();
            browser.sleep(2000);

        });

    };

    this.clickConfirmDelete = function () {
        element(by.id('btn-delete-app')).click();
        browser.sleep(3000);

    };

    this.verifyDeleteApplicationSuccessfully = function (appName) {
        element(by.binding(appName)).isPresent(false);
        browser.sleep(1000);
    }




    this.verifyEditSuccessfully = function () {

        var datas = element.all(by.repeater('app in vm.allApps')).map(function (row) {
            return {
                appNameMap: row.$$('td').get(0).getText(),
                sessionExpirationMap: row.$$('td').get(2).getText(),
                userBaseMap: row.$$('td').get(3).getText()
            }
        });
        datas.then(function (datas) {
            var isExisted = false;
            for (var i = 0; i < datas.length; i++) {
                if (datas[i].appNameMap === appNameKeeper) {
                    isExisted = true;
                    expect(datas[i].sessionExpirationMap).to.be.equal(sessionExpirationKeeper);
                    expect(datas[i].userBaseMap).to.be.equal(userBaseKeeper);
                }
            }
            if (!isExisted) {
                throw Error('Application does not exist');
            }
        })


    };

    this.editApplicationInfoSuccessfully = function (originalApplicationName, modifyApplicationName, sessionExpiration, userBase) {
        appNameKeeper = modifyApplicationName;
        sessionExpirationKeeper = sessionExpiration;
        userBaseKeeper = userBase;
        element.all(by.repeater('app in vm.allApps')).filter(function (rows) {
            return rows.all(by.css('td')).then(function (columns) {
                return columns[0].getText().then((function (text) {
                    console.info('originalApplicationName ' + originalApplicationName);
                    return text === originalApplicationName;
                }))
            })
        }).then(function (filterElements) {
            filterElements[0].element(by.linkText('Edit')).click().then(function () {
                element.all(by.model('vm.app.name')).get(1).clear();
                element.all(by.model('vm.app.name')).get(1).sendKeys(modifyApplicationName);
                browser.sleep(500);
                element.all(by.model('vm.app.sessionExpiration')).get(1).clear();
                element.all(by.model('vm.app.sessionExpiration')).get(1).sendKeys(sessionExpiration);
                browser.sleep(500);
                element.all(by.model('vm.app.userBase')).get(1).sendKeys(userBase);
                element(by.buttonText('Update')).click();
                browser.sleep(2000);
            });
        });
    };

};
module.exports = ApplicationPage;