Feature: Login Dashboard
  As a user,
  I want to login to Dashboard
  So that I have to enter username and password firstly

  Scenario Outline: Login Dashboard successfully
    Given I open the website <website>
    Then I verify that login screen appears
    When I type username as <username> , password as <password>
    And I click Login button
    Then I check dashboard screen appears
    Examples:
      | website               | username | password |
      | http://localhost/demo | tixiuthu | 123456   |
      | http://localhost/demo | axon     | 123456   |
