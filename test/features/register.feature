# Created by nguyenthihongthuy at 15/05/16
Feature: Register Account
  As a user of Fusion website,
  I want to create an account
  So that I have to login the system

  Scenario Outline: Register an ccount successfully
    Given I open the website <website>
    Then I verify that welcome page is appeared
    When I click Register button
    Then I verify that register page is appeared
    When I register with firstname <firstname>, lastname <lastname>, username <username>, password <password>
    Then I verify that system shows the <message>

    Examples:
      | website                | firstname | lastname | username | password | message                 |
      | http://localhost/demo/ | Thuy      | Nguyen   | tixiuthu | 123456   | Registration successful |
      | http://localhost/demo/ | Axon      | Active   | axon     | 123456   | Registration successful |
