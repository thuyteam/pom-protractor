# Created by nguyenthihongthuy at 15/05/16
Feature: Add and Delete Application
  As a user of Fusion website,
  I want to work in fusion's system
  So that I have to login the system firstly

  Scenario Outline: Add Application
    Given I login in <website> with username <username>, password <password> successfully
    When I navigate to Application page
    Then I click on Add Application button
    When I added an application with appname <appName>, session expiration <session>, userBase <userBase>
    Then I verify the application with <appName> is added successfully

    Examples:
      | website               | username | password | appName    | session | userBase                 |
      | http://localhost/demo | tixiuthu | 123456   | Hang       | 50      | User By Location Sharing |
      | http://localhost/demo | admin    | admin    | SwissArena | 150     | User By Session          |
      | http://localhost/demo | admin    | admin    | SMT Smart  | 150     | User By Session          |

  Scenario Outline: Edit an application successfully
    Given I login in <website> with username <username>, password <password> successfully
    When I navigate to Application page
    And I edit application infomation of <originalApplicationName> with application name as <modifyApplicationName>, Session Expiration as <sessionExpiration>,User Base as <userBase>
    Then I verify the application with <originalApplicationName> is edited successfully
    Examples:
      | website               | username | password | originalApplicationName | modifyApplicationName | sessionExpiration | userBase        |
      | http://localhost/demo | tixiuthu | 123456   | SwissArena              | SwissArena modified   | 130               | Both            |
      | http://localhost/demo | admin    | admin    | SMT Smart               | SMT Smart modified    | 200               | User By Session |


  Scenario Outline: Delete Application
    Given I login in <website> with username <username>, password <password> successfully
    When I navigate to Application page
    And I delete app name as <appName>
    Then I verify the application with <appName> is disappeared

    Examples:
      | website               | username | password | appName             |
      | http://localhost/demo | tixiuthu | 123456   | SwissArena modified |
      | http://localhost/demo | admin    | admin    | SMT Smart modified  |


